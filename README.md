# Drumkit Drupal 8

This is a Hugo docs "content pack" from Consensus Enterprises. You can pull it
into your Hugo site as a git submodule:

`git submodule add https://gitlab.com/consensus.enterprises/docs/drumkit-drupal-8.git content/dev/drumkit-drupal-8`
