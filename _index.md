---
title: Drumkit & Drupal 8
weight: 10
---

Initialize a new project:

```
mkdir $project ; cd $project ; git init
touch README ; git add README ; git commit -m "Initial commit."
wget -O - https://drumk.it/installer | /bin/bash
git add .
git commit -m "Install Drumkit."
make init-project-drupal8
```
